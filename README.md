# Markdown2HTML
---
## What is it?

As the description says, it is a very bad markdown-to-HTML converter.

## How do I use it?

If you're serious about converting markdown to HTML, I strongly suggest you do not use this.
If you're commited to using this, here's how it goes

I have made it so the program takes markdown directly from STDIN.
This is good because now you can actually use it with unix pipes
to get stuff done. Failure to provide a file will make the program
interactive, meaning that you can enter markdown like a REPL
and get HTML out.

The program takes the data from STDIN, checks for syntax on a line-by-line
basis, does regex substitutions appropriately, and sends the resulting
HTML to STDOUT (meaning you can pipe the results to a file).

Example usage:
`./md2html.pl < tobetranslated.md > mytranslatedfile.html`

The test file I use has also been provided as test.md

Everything in it should work. If something in it doesn't get translated, then something is wrong.

The syntax currently supported is:
* Italic text (\* to \<em>)
* Bold text (\*\* to \<strong>)
* The combination of the above (\*\*\*)
* Monospace text (\` to \<pre>)
* Deleted text (\~\~ to \<del>)
* Headers (\# to \<h(1,2,..,6))
* Horizontal rules (\--- to \<hr>) (Has to be in a line by itself)
* Blank lines get turned into \<br> (I know this isn't exactly the 
behavior needed, I'll see what I can do)
* Inline links (with or without a title) (\<a href=...>)
* Images
* Escaping characters with the backslash is supported

#!/usr/bin/perl -n

BEGIN: {
    use strict;
    use warnings;
}

chomp $_;
# Horizontal rule
$_ =~ s/^((?<!\\)-){3}$/<hr>/g;
# Line break
$_ =~ s/^$/<br>/g;

# Bold text
$_ =~ s/((?<!\\)\*){2}(.+)((?<!\\)\*){2}/<strong>$2<\/strong>/g;
# Italics
$_ =~ s/((?<!\\)\*)(.+)((?<!\\)\*)/<em>$2<\/em>/g;
# Strikethrough
$_ =~ s/((?<!\\)\~){2}(.+)((?<!\\)\~){2}/<del>$2<\/del>/g;
# Inline monospace
$_ =~ s/((?<!\\)\`)(.+)((?<!\\)\`)/<pre>$2<\/pre>/g;

# Images
$_ =~ s/(?<!\\)\!(?<!\\)\[(.+)\](?<!\\)\((.+) +\"(.+)\"\)/<img src="$2" alt="$1" title="$3">/g;

# Inline links with title
$_ =~ s/(?<!\\)\[(.+)\](?<!\\)\((.+) +\"(.+)\"\)/<a href="$2" title="$3">$1<\/a>/g;
# Inline links without title
$_ =~ s/(?<!\\)\[(.+)\](?<!\\)\((.+)\)/<a href="$2">$1<\/a>/g;

# Headers
$_ =~ s/^#{6}\s+(.+)/<h6>$1<\/h6>/g;
$_ =~ s/^#{5}\s+(.+)/<h5>$1<\/h5>/g;
$_ =~ s/^#{4}\s+(.+)/<h4>$1<\/h4>/g;
$_ =~ s/^#{3}\s+(.+)/<h3>$1<\/h3>/g;
$_ =~ s/^#{2}\s+(.+)/<h2>$1<\/h2>/g;
$_ =~ s/^#{1}\s+(.+)/<h1>$1<\/h1>/g;

# Escaped character handling
$_ =~ s/(?<!\\)\\(.)/$1/g;

print "$_\n";


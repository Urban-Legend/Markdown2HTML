*italic text*
**bold text**
***bold italics***

\* escaping *test*
back\\test

# H1
## H2
### H3
#### H4
##### H5
###### H6

[Here's a link to someplace](https://www.youtube.com/watch?v=-CbxUk8QX9M)
[Here's a link with a title](https://www.youtube.com/watch?v=-CbxUk8QX9M "title test")

![Image test](https://makefrontendshitagain.party/_nuxt/img/3e7d38b.gif "orbiting rocket")

---

~~erased~~
`monospaced`
